function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

var arrInput = document.getElementsByTagName("input");
for(var i = arrInput.length; i--; ){
    var el = arrInput[i];
    if(el.getAttribute("type") == "text"){
        var dataHint = el.getAttribute("data-hint");
        if(dataHint != null){
            var tmpSpan = document.createElement("span");
            tmpSpan.setAttribute("title", dataHint);
            tmpSpan.innerHTML = "?";
            console.log(tmpSpan);

            insertAfter(el, tmpSpan)
        }
    }
}
