function error_msg(inp, msg){
    inp.setCustomValidity(msg);
}

function checkStr(valFromInp, maxR, minR){
    var minNotNull = minR != null;
    if(valFromInp == "" && minNotNull){
        return "Введите пожалуйста данные";
    }else if(isNaN(parseInt(valFromInp)) && minNotNull){
        return "Введите число";
    }
    var len = valFromInp.length;
    if(minNotNull){
        minR = parseInt(minR, 10);
        if(minR > len){
            return "Минимальная длина поля " + minR;
        }
    }
    if(maxR != null){
        maxR = parseInt(maxR, 10);
        if(len > maxR){
            return "Максимальная длина поля " + maxR;
        }
    }
    return null;
}

var butt_click = function (){
    var arrInput = document.getElementsByTagName("input");
    for(var i = 0; i < arrInput.length; ++i){
        var el = arrInput[i];
        if(el.getAttribute("extype") != null){
            error_msg(el, ""); // reset
            var msg = checkStr(el.value, 
                               el.getAttribute("maxrange"), el.getAttribute("minrange"));
            var span = document.getElementsByTagName('span')[i];
            span.textContent = (msg !== null) ? msg : '';
        }
    }   
}