package by.psu;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/add")
public class ControllerAdd extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Person> list = new XMLParser("R:\\jobs\\clone\\laboratory7\\persons.xml").parseXML();
        req.setCharacterEncoding("UTF8");
        int id = list.get(list.size() - 1).getId() + 1;
        String name=req.getParameter("name");
        String surname=req.getParameter("surname");
        String date=req.getParameter("date");
        String number=req.getParameter("number");
        String city=req.getParameter("city");
        String address=req.getParameter("address");

        boolean check = true;
        for (Person per : list) {
            if((per.getName() + " " + per.getLastName()).equalsIgnoreCase(name + " " + surname)){
                check = false;
                break;
            }
        }

        if(!check){
            req.getSession().setAttribute("error", "Пользователь с таким ФИ существует");
        } else if((new XMLParser("R:\\jobs\\clone\\laboratory7\\persons.xml")).isPersonById(id)){
            req.getSession().setAttribute("error", "Пользователь с таким id существует");
        } else {
            Person person = new Person(id, name, surname, date, number, city, address);
            new XMLParser("R:\\jobs\\clone\\laboratory7\\persons.xml").saveToXML(person);
            req.getSession().setAttribute("error", "Ошибок нет");
        }


        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }
}
