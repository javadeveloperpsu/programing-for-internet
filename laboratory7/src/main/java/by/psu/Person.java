package by.psu;

import lombok.Getter;
import lombok.Setter;


public class Person {
    @Getter @Setter private int id;
    @Getter @Setter private String name;
    @Getter @Setter private String lastName;
    @Getter @Setter private String birthday;
    @Getter @Setter private String phoneNumber;
    @Getter @Setter private String city;
    @Getter @Setter private String address;

    public Person() { }

    public Person(int id, String name, String lastName, String birthday, String phoneNumber, String city, String address) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.birthday = birthday;
        this.phoneNumber = phoneNumber;
        this.city = city;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday='" + birthday + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
