package by.psu;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class XMLParser {

    private DocumentBuilder documentBuilder = null;
    private Document document = null;

    private String xmlFile;

    public XMLParser(String xmlFile){
        this.xmlFile = xmlFile;
        try {
            documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = documentBuilder.parse(xmlFile);
        } catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
    }

    public List<Person> parseXML(){
        StringBuilder stringBuilder = new StringBuilder();
        List<Person> personList = new ArrayList<>();

        Node root = document.getDocumentElement();
        NodeList nodeContacts = root.getChildNodes();
        for (int i = 0; i < nodeContacts.getLength(); i++) {
            Person person = new Person();
            Node node = nodeContacts.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                person.setId(Integer.parseInt(node.getAttributes().getNamedItem("id").getTextContent()));
            }
            NodeList nodeList = node.getChildNodes();
            String[] argc = null;
            for (int j = 0; j < nodeList.getLength(); j++) {
                Node node1 = nodeList.item(j);
                if (node1.getNodeType() == Node.ELEMENT_NODE) {
                    stringBuilder.append(node1.getTextContent()).append("~");
                }
            }
            if(stringBuilder.length()!= 0) {
                stringBuilder = stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                argc = String.valueOf(stringBuilder).split("~");

                System.out.println("argc[0] = " + argc[0] + "argc[1] = " + argc[1] + "argc[2] = "
                + "argc[2] = " + argc[2] + "argc[3] = " + argc[3] + "argc[4] = " + argc[4] + "argc[5] = " + argc[5]);
                person.setName(argc[0]);
                person.setLastName(argc[1]);
                person.setBirthday(argc[2]);
                person.setPhoneNumber(argc[3]);
                person.setCity(argc[4]);
                person.setAddress(argc[5]);

                personList.add(person);

                System.out.println(person);

                stringBuilder.setLength(0);
            }
        }
        return personList;
    }



    public void saveToXML(Person person) {
        try {

            Node root = document.getDocumentElement();

            // staff elements
            Element staff = document.createElement("person");
            root.appendChild(staff);

            // set attribute to staff element
            Attr attr = document.createAttribute("id");
            attr.setValue(String.valueOf(person.getId()));
            staff.setAttributeNode(attr);

            Element name = document.createElement("name");
            name.appendChild(document.createTextNode(person.getName()));
            Element lastName = document.createElement("lastName");
            lastName.appendChild(document.createTextNode(person.getLastName()));
            Element birthday = document.createElement("birthday");
            birthday.appendChild(document.createTextNode(person.getBirthday()));
            Element phoneNumber = document.createElement("phoneNumber");
            phoneNumber.appendChild(document.createTextNode(person.getPhoneNumber()));
            Element city = document.createElement("city");
            city.appendChild(document.createTextNode(person.getCity()));
            Element address = document.createElement("address");
            address.appendChild(document.createTextNode(person.getAddress()));

            staff.appendChild(name);
            staff.appendChild(lastName);
            staff.appendChild(birthday);
            staff.appendChild(phoneNumber);
            staff.appendChild(city);
            staff.appendChild(address);

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(xmlFile));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);

            transformer.transform(source, result);

            System.out.println("File saved!");

        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    public boolean isPersonById(int id){
        Node root = document.getDocumentElement();
        NodeList nodeContacts = root.getChildNodes();
        for (int i = 0; i < nodeContacts.getLength(); i++) {
            Node node = nodeContacts.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                if( Integer.parseInt(node.getAttributes().getNamedItem("id").getTextContent()) == id ){
                    return true;
                }
            }
        }
        return false;
    }
}