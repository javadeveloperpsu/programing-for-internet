package by.psu;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        XMLParser xmlParser = new XMLParser("persons.xml");
        List<Person> personList = xmlParser.parseXML();
        personList.forEach(System.out::println);

        /*Person person = new Person();
        person.setId(2);
        person.setName("Алексей");
        person.setLastName("Пронько");
        person.setBirthday("17/12/1997");
        person.setPhoneNumber("+375294785695");
        person.setCity("Новополоцк");
        person.setAddress("Улица Молодежная д 69");*/

        //xmlParser.saveToXML(person);
    }

}
