<%@ page import="by.psu.Person" %>
<%@ page import="by.psu.XMLParser" %><%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title></title>
  </head>
  <body>
  <% for(Person p: (new XMLParser("R:\\jobs\\clone\\laboratory7\\persons.xml")).parseXML()){ %>
    <p><%=p.getId()%>
    <%=p.getName()%>
    <%=p.getLastName()%>
    <%=p.getBirthday()%>
    <%=p.getPhoneNumber()%>
    <%=p.getCity()%>
    <%=p.getAddress()%>
    </p>
  <% } %>
    <table border="1" width="100%">
      <tr>
        <th>id</th>
        <th>Имя</th>
        <th>Фамилия</th>
        <th>Дата</th>
        <th>Мобильный номер</th>
        <th>Город</th>
        <th>Адрес</th>
      </tr>
      <% for(Person p: (new XMLParser("R:\\jobs\\clone\\laboratory7\\persons.xml")).parseXML()){ %>
      <tr>
        <td><%=p.getId()%></td>
        <td><%=p.getName()%></td>
        <td><%=p.getLastName()%></td>
        <td><%=p.getBirthday()%></td>
        <td><%=p.getPhoneNumber()%></td>
        <td><%=p.getCity()%></td>
        <td><%=p.getAddress()%></td>
      </tr>
      <% } %>
    </table>
    <form action="add" method="post">
      <input type="number" placeholder="id" name="id">
      <input type="text" placeholder="Имя" name="name">
      <input type="text" placeholder="Фамилия" name="surname">
      <input type="date" placeholder="Дата рождения" name="date">
      <input type="text" placeholder="Мобильный телефон" name="number">
      <input type="text" placeholder="Город" name="city">
      <input type="text" placeholder="Адрес" name="address">
      <input type="submit" value="Добавить">
    </form>
  <%=(String)session.getAttribute("error")%>
  </body>
</html>